#pragma once
#ifndef _MAP_H_
#define _MAP_H_

#include "structs.hpp"
#include <array>

struct MapDoor;
struct MapRoom;


class Map final
{
public:
	explicit Map();
	explicit Map(const std::string& name, bool rooms, const Position& position = { -1, -1 });
	Map(Map&& other) noexcept;
	Map(const Map& other) = delete;
	Map& operator=(const Map& other) = delete;
	Map& operator=(Map&& other) noexcept = delete;
	~Map();

	void LoadMap(std::wifstream& file);
	void GenerateMap(const Position& position);
	void SaveMap();

	void Draw(const Position& playerPos) const;

	//int GetId(int x, int y) const;
	int CheckTile(int x, int y) const;
	int CheckTile(const Position& pos) const;
	std::string GetName() const;
	bool IsRoomLevel() const;

	static constexpr std::array<wchar_t, 6> WALLS_ARRAY = { L'\x2514', L'\x250c', L'\x2510', L'\x2518', L'\x2500', L'\x2502' };
	Position m_DownStairsPos;

private:
	std::string m_MapName;
	std::wstring* m_pTiles;
	//std::vector<ItemTile> m_Items;
	bool m_IsRooms;


	void BresenhamMap(int x1, int y1, const int x2, const int y2);
};

/* MAP INFO */
//extern struct Map CURRENT_MAP;

#endif // !MAP_H
