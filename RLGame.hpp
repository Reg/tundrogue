#pragma once
#ifndef _RLGAME_H_
#define _RLGAME_H_

#include "TerminalWrapper.hpp"
#include "structs.hpp"
#include "Map.hpp"
#include "ItemList.hpp"
#include <bitset>

/// GLOBAL VARIABLES ///
// globals get G_ prefix
inline bool G_QUIT{ false };
//inline unsigned long G_TURNS{};
inline std::string G_STATUS_TEXT{};
inline std::string G_FULL_TEXT{};
inline int G_OFFSET_X{};
inline int G_OFFSET_Y{};
inline int G_OFFSET_PREV_X{};
inline int G_OFFSET_PREV_Y{};
inline Map* G_CURRENT_MAP;
inline ItemList* G_ITEM_LIST{ new ItemList() };
inline short G_MESSAGE_COUNT{};

/// COLORS ///
// BearLibTerminal uses a 32-bit unsigned int for colors
// 0x AA RR GG BB
inline constexpr color_t G_COL_BLACK		{ 0xFF000000u };
inline constexpr color_t G_COL_WHITE		{ 0xFFFFFFFFu };
inline constexpr color_t G_COL_GRAY			{ 0xFFB2B2B2u };
inline constexpr color_t G_COL_DARKEST_GRAY	{ 0xFF121212u };
inline constexpr color_t G_COL_AMBER		{ 0xFFFF9200u };
inline constexpr color_t G_COL_SEA			{ 0xFF00FF7Fu };

/// LAYERS (for drawing stuff) ///
inline constexpr unsigned short G_LAYER_MAIN	{ 0 }; // map and characters get drawn here
inline constexpr unsigned short G_LAYER_MENU	{ 2 }; // layer 1 is also reserved for MAIN like MENU_TOP is for MENU, I think?
inline constexpr unsigned short G_LAYER_MENU_TOP{ 3 };
inline constexpr unsigned short G_LAYER_STATUS	{ 4 }; // info at the bottom of the screen

/// CHEATS ///
// cheats get CH_ prefix
enum G_CH_CODES : unsigned int
{
	SECRET,
	FUNNYMODE,
	GHOST,
	GOD
};

inline std::bitset<GOD + 1> G_CH_BITS{};

/*
inline bool G_SECRET{ false }; // except this one
inline bool G_CH_FUNNY_MODE{ false }; // wacky colors for walls
inline bool G_CH_NOCLIP{ false };

#ifdef _DEBUG
	inline bool G_CH_GOSH{ true };
#else
	inline bool G_CH_GOSH{ false };
#endif // _DEBUG*/

/// MAP INFO ///
constexpr wchar_t G_MAPTILE_WALL	{ L'█' };
constexpr wchar_t G_MAPTILE_CORRIDOR{ L'#' };
constexpr wchar_t G_MAPTILE_DOOR	{ L'+' };
//constexpr wchar_t MAPTILE_CORRIDOR{ L'░' }; // variants
//constexpr wchar_t MAPTILE_DOOR	{ L'┼' };

constexpr unsigned int G_MAP_WIDTH	{ 248 };
constexpr unsigned int G_MAP_HEIGHT	{ 248 };

/// SCREEN INFO ///
constexpr unsigned short G_SCREEN_WIDTH	{ 124 };
constexpr unsigned short G_SCREEN_HEIGHT{ 44 };
constexpr unsigned short G_SCREEN_BOTTOM{ 3 };	// status bar

/// KEYS (for pdcurses) ///
//constexpr char KEY_ESC{ 27 };
//constexpr char KEY_DOWNLEFT_PAD{ 49 };
//constexpr char KEY_DOWN_PAD{ 50 };
//constexpr char KEY_DOWNRIGHT_PAD{ 51 };
//constexpr char KEY_LEFT_PAD{ 52 };
//constexpr char KEY_RIGHT_PAD{ 54 };
//constexpr char KEY_UPLEFT_PAD{ 55 };
//constexpr char KEY_UP_PAD{ 56 };
//constexpr char KEY_RIGHTUP_PAD{ 57 };

/// FUNCTIONS ///
void G_QUIT_GAME(const std::string& err = "");
void G_PRINT_STATUS();
void G_PRINT_STATUS(const std::string& string);
void G_PRINT_STATUS_SAMELINE(const std::string& string);
/*int GET_AT_POS(const int x, const int y);
int GET_AT_POS(const Position& pos);*/

#endif // !RLGAME_H

/* rule of five
explicit CLASSNAME();
CLASSNAME(const CLASSNAME& other);
CLASSNAME(CLASSNAME&& other) noexcept;
CLASSNAME& operator=(const CLASSNAME& other);
CLASSNAME& operator=(CLASSNAME&& other) noexcept;
*/
