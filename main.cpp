#include "Game.hpp"
#include "RLGame.hpp"
#include <iostream>
#include <fstream>
#include <ctime>
#include <locale>

#ifdef _WIN32
#pragma comment(lib, "libraries/lua542/lua54.lib")
#endif // _WIN32

void ShowInstructions()
{
#ifdef _DEBUG
	std::cout << "DEBUG build\n";
#endif // _DEBUG

	/*
	std::cout << "-- TUNDROGUE --");
	std::cout << "Controls:");
	std::cout << "- arrow keys or numpad: move");
	std::cout << "- numpad 5 or period(.): wait");
	std::cout << "- shift-f: attack");
	if(!G_CH_BITS[G_CH_CODES::SECRET])
		std::cout << "- shift-h: hug");
	else
		std::cout << "- shift-h: frick");
	std::cout << "- < / shift-PgUp: go up");
	std::cout << "- > / shift-PgDown: go down");
	std::cout << "- i: open inventory and use item");
	std::cout << "- g: grab item");
	std::cout << "- d: drop item");
	std::cout << "- e: print equipped items");
	std::cout << "- p: print character stats");
	std::cout << "- b: print all previous status messages");
	std::cout << "- ESC: quit\n");
	std::cout << "QUEST: Defeat the human");
	std::cout << "Generated maps are saved to \"resources/maps\"\n");
	*/

	std::string mesg{};
	std::ifstream mesgFile{ "data/intro.txt" };
	if(mesgFile.is_open())
	{
		/*std::string line{};
		while(std::getline(mesgFile, line))
		{
			mesg += line + '\n';
		}*/
		mesgFile.seekg(0, std::ios::end);
		mesg.resize(mesgFile.tellg());
		mesgFile.seekg(0, std::ios::beg);
		mesgFile.read(&mesg[0], mesg.size());
		mesgFile.close();
	}
	else
	{
		mesg += "Couldn't open \"data\\intro.txt\"";
	}
	std::cout << mesg << '\n';

#ifdef _DEBUG
	std::cout	<< "Debug controls:\n"
				<< "- F1: spawn an NPC\n"
				<< "- F2: draw a line to a random NPC\n"
				<< "- F3: save current map\n"
				<< "- F4: add random item to inventory\n"
				<< "- F5: suicide\n";
#endif // _DEBUG
	std::cout << std::endl;
}

int main(int argc, char* argv[])
{
	srand(static_cast<unsigned int>(time(nullptr)));
	std::locale::global(std::locale("en_US.utf8"));
	//std::locale::global(std::locale("C"));
	//std::setlocale(LC_ALL, "");

	if(terminal_open() == false)
	{
		std::cout << "ERROR: BearLibTerminal could not be initialized" << std::endl;
		return -1;
	}
	else
	{
		if(argc == 2)
		{
			std::string secretString{ argv[1] };
			int answer{};
			// this way the secret string isn't just written in the executable
			for(char letter : secretString)
				answer += letter;

			if(answer == 2166)
			{
				std::cout << "SECRET enabled\n";
				G_CH_BITS[G_CH_CODES::SECRET] = true;
				//G_SECRET = true;
			}
			//srand(static_cast<unsigned int>(answer)); // don't remember why I added this
		}

		ShowInstructions();
		terminal_setf("window: title=TundRogue, size=%dx%d, icon=./resources/face.ico, resizeable=false;", G_SCREEN_WIDTH, G_SCREEN_HEIGHT);
		terminal_set("input.filter=[keyboard, keypad, arrows];");
		Game game{};
		game.Run();
	}

	terminal_close();
	std::cout << "Press ENTER to quit\n" << std::endl;
	std::cin.get();

	return 0;
}
