#include "RLGame.hpp"
#include "utils.hpp"
#include <iostream>

/*
 *	FUNCTIONS
 */

// quit the game, if err != "" then an error is printed
void G_QUIT_GAME(const std::string& err)
{
	G_QUIT = true;
	//terminal_clear();
	if(err != "")
	{
		terminal_layer(G_LAYER_MAIN);
		terminal_printf(0, 0, "[bkcolor=red]%s", err.c_str());
		std::cerr << err;
		const char mesg[]{ "Press ENTER to close the console" };
		terminal_print(0, 1, mesg);
		std::cerr << '\n' << mesg << std::endl;
		terminal_refresh();
		utils::WaitForEnter();
		//while(terminal_read() != TK_ENTER || terminal_read() != TK_KP_ENTER); // wait until enter is pressed
	}
}

void G_PRINT_STATUS()
{
	//G_FULL_TEXT += G_STATUS_TEXT + '\n';
	terminal_layer(G_LAYER_STATUS + 1);
	terminal_printf(0, G_SCREEN_HEIGHT - 1, "%s", G_STATUS_TEXT.c_str());
	//terminal_printf(0, 0, "%s", G_STATUS_TEXT.c_str());
	terminal_layer(0);
}

void G_PRINT_STATUS(const std::string& string)
{
	//G_FULL_TEXT += G_STATUS_TEXT + '\n';
	G_STATUS_TEXT = string;
	terminal_layer(G_LAYER_STATUS + 1);
	terminal_printf(0, G_SCREEN_HEIGHT - 1, "%s", G_STATUS_TEXT.c_str());
	G_FULL_TEXT += G_STATUS_TEXT + '\n';
	terminal_layer(0);
	terminal_refresh();
}

void G_PRINT_STATUS_SAMELINE(const std::string& string)
{
	terminal_layer(G_LAYER_STATUS + 1);
	if(G_MESSAGE_COUNT >= 2)
		G_STATUS_TEXT.clear();
	G_STATUS_TEXT += string + ' ';
	++G_MESSAGE_COUNT;
	terminal_printf(0, G_SCREEN_HEIGHT - 1, "%s", G_STATUS_TEXT.c_str());
	terminal_layer(0);
	terminal_refresh();
}

// Returns the char at the given position (not on the level but the actual char currently drawn on screen)
/*int GET_AT_POS(const int x, const int y)
{
	return terminal_pick(x, y);
}

int GET_AT_POS(const Position& pos)
{
	return GET_AT_POS(pos.x, pos.y);
}*/
