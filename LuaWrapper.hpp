#pragma once
#ifndef lua_h // use lua's define guard

extern "C"
{
#include "libraries/lua542/include/lua.h"
#include "libraries/lua542/include/lauxlib.h"
#include "libraries/lua542/include/lualib.h"
}

#endif // lua_h
