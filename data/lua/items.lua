items =
{
	{
		name = "McGuffin",
		value = 15,
		mass = 5
	},
	{
		name = "Ring",
		value = 15,
		mass = 5
	}
}

consumables =
{
	{
		name = "Pizza",
		value = 50,
		mass = 1,
		heal = 15
	},
	{
		name = "Pasta",
		value = 50,
		mass = 1,
		heal = 15
	},
    {
		name = "Health Potion",
		value = 10,
		mass = 1,
		heal = 15
	}
}

weapons =
{
	{
		name = "Sword",
		value = 35,
		mass = 4,
		minDamage = 5,
		maxDamage = 15
	},
	{
		name = "Spear",
		value = 35,
		mass = 3,
		minDamage = 7,
		maxDamage = 18
	},
    {
		name = "Goedendag",
		value = 45,
		mass = 5,
		minDamage = 8,
		maxDamage = 25
	}
}

armors =
{
    {
		name = "Bucket Hat",
		value = 1,
		mass = 2,
		rating = 2
	},
	{
		name = "Cowboy Hat",
		value = 15,
		mass = 1,
		rating = 5
	},
    {
		name = "Winter Coat",
		value = 25,
		mass = 2,
		rating = 6
	}
}
