#pragma once
#ifndef _STRUCTS_H_
#define _STRUCTS_H_

struct Position
{
	int x;
	int y;

	bool operator==(const Position& rhs) const;
};

#endif // !_STRUCTS_H_
