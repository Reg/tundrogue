#pragma once
#ifndef _UTILS_H_
#define _UTILS_H_

//#include "structs.hpp"
//#include <map>
//#include <queue>
//#include <vector>
#include <string>

struct Position;
typedef unsigned int color_t;

namespace utils
{
	bool IsOverlapping(int x1, int y1, int width1, int height1, int x2, int y2, int width2, int height2);
	void Bresenham(int x1, int y1, const int x2, const int y2);

	int RandomBetween(const int min, const int max);
	int RandomBetweenSimple(const int min, const int max);
	color_t RandomColor();
	Position RandomPositionBetween(const int minX, const int maxX, const int minY, const int maxY);

	void DrawBox(int x, int y, int width, int height, int layer, bool background, const std::string& title = "", color_t foregroundColor = color_t(UINT32_MAX), color_t backgroundColor = color_t(0xFF000000u)); // white and black
	void ClearBox(int x, int y, int width, int height, int layer);

	wchar_t GetWChar();
	int GetKey();
	bool WaitForEnter();
	//size_t SplitString(const std::string& toSplit, std::vector<std::string>& splitString, char delimiter = ' ');
	//std::vector<Position> BFS(const Position& start, const Position& end);
	//std::map<Position, Position> Pathfind(const Position& start, const Position& end);
	//std::vector<Position> GetNeighbors(const Position& from);
}
#endif // !_UTILS_H_
