#pragma once
#ifndef _NPC_H_
#define _NPC_H_

#include "Entity.hpp"

class NPC : public Entity
{
public:
	explicit NPC();
	explicit NPC(const std::string& name, SPECIES type, Position position);

	void Update(const std::vector<Entity*>& entities) override;
private:
	void CheckInventory(ITEM_TYPE itemType);
};

#endif // !NPC_H
