#pragma once
#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <vector>
#include "Item.hpp"

enum class SPECIES : short
{
	LIZARD,
	KOBOLD,
	DRAGON,
	WYVERN,
	HYDRA,
	EEL,
	SNAKE,
	CRAB,
	HUMAN,
	MAX = HUMAN,
	UNDEFINED = -1
};

enum class DIRECTION : short
{
	NORTH,
	SOUTH,
	EAST,
	WEST,
	NORTHEAST,
	SOUTHEAST,
	NORTHWEST,
	SOUTHWEST
};

enum class ATTRIBUTES : short
{
	STRENGTH,
	INTELLECT,
	DEFTNESS
};

enum class ENTITY_FACTION : short
{
	NEUTRAL,
	FRIENDLY,
	DEVOTED,
	EVIL
};

/*
struct Stats
{
};
*/

class Entity
{
public:
	explicit Entity();
	explicit Entity(const std::string& name, SPECIES type, Position position);
	Entity(const Entity& other) = delete;
	Entity(Entity&& other) noexcept = delete;
	Entity& operator=(const Entity& other) = delete;
	Entity& operator=(Entity&& other) noexcept = delete;
	virtual ~Entity();

	/// MAIN THINGS ///
	virtual void Draw() const;
	virtual void Update();
	virtual void Update(const std::vector<Entity*>& entities);
	bool Fight(Entity& other);
	bool Hug(Entity& other);
	bool UseItem(unsigned int index);
	std::string PrintSheet() const;

	/// GETTERS ///
	const std::string& GetName() const;
	const std::string GetFullName() const;
	char GetSymbol() const;
	SPECIES GetSpecies() const;
	const char* GetSpeciesString() const;
	const Position& GetPosition() const;
	const int& GetHealth() const;
	const int& GetMaxHealth() const;
	const int& GetAttribute(ATTRIBUTES attribute) const;
	bool IsDead() const;
	int GetDistanceFrom(const Position& pos) const;
	int GetDistanceFrom(const Entity& other) const;
	std::vector<Entity*> GetClosest(const std::vector<Entity*>& entities, int range = 1);
	std::vector<Entity*> GetClosestWithState(const std::vector<Entity*>& entities, ENTITY_FACTION state, int range = 1);
	unsigned int GetColor() const;
	int GetHug() const;
	ENTITY_FACTION GetFaction() const;
	Weapon* GetWeapon() const;
	Armor* GetArmor() const;

	/// SETTERS ///
	void SetName(const std::string& newName);
	void SetSpecies(const SPECIES& type);
	virtual void SetPosition(int x, int y);
	virtual void SetPosition(Position position);
	void SetHealth(int health);
	void SetMaxHealth(int health);
	void SetAttribute(ATTRIBUTES attribute, int value);
	void SetHug(int value);
	void SetFaction(ENTITY_FACTION state);

	/// OTHER ///
	void AddHealth(int health);
	virtual bool Move(int x, int y);
	virtual bool Move(DIRECTION direction);
	void MoveTo(Position position);
	void MoveTo(const Entity& other);
	void AddAttribute(ATTRIBUTES attribute, int amount);
	void AddHug(int amount);

	/// INVENTORY THINGS ///
	std::vector<Item*>& GetInventory();
	void AddItem(Item* item);
	void RemoveItem(unsigned int index);
	bool DropItem(std::vector<ItemTile>& items, unsigned int index);

protected:
	static Entity* m_pPlayer;

	SPECIES m_Species;
	Position m_Pos;
	Position m_PreviousPos;
	bool m_Dead;
	bool m_UpperChar;
	int m_MaxHealth;
	int m_Health;
	int m_AttrStrength;
	int m_AttrIntellect;
	int m_AttrDeftness;
	int m_HugCounter;
	ENTITY_FACTION m_Faction;

	Weapon* m_pWeapon;
	Armor* m_pArmor;
	std::vector<Item*> m_Inventory;
	std::string m_Name;

	virtual void Die();

	// just load from ini file instead, allows to be edited
	// maybe a bit slower

	//const static color_t m_ColorLizard;
	//const static color_t m_ColorKobold;
	//const static color_t m_ColorDragon;
	//const static color_t m_ColorWyvern;
	//const static color_t m_ColorHydra;
	//const static color_t m_ColorEel;
	//const static color_t m_ColorHuman;
};

#endif // !_ENTITY_H_
