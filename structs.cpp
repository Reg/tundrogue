#include "structs.hpp"

/// POSITION ///

bool Position::operator==(const Position& rhs) const
{
	return (x == rhs.x && y == rhs.y);
}

//bool Position::operator!=(const Position& rhs)
//{
//	return *this == rhs;
//}
