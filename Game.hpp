#pragma once
#ifndef _GAME_H_
#define _GAME_H_

#include <vector>
#include "Player.hpp"

class Entity;
class NPC;
class Room;
class Map;

class Game final
{
public:
	explicit Game(/*const char* title, bool stuff*/);
	Game(const Game& other) = delete;
	Game(Game&& other) noexcept = delete;
	Game& operator=(const Game& other) = delete;
	Game& operator=(Game&& other) noexcept = delete;
	~Game();

	void Run();

private:
	void Init();
	void ShutDown();
	void Load();
	void Save();
	int GetKey(bool wait = true) const;
	void Update();
	void Draw() const;
	void UpdateRealTime(float elapsedTime);
	void DrawRealTime() const;
	void HandleInput(int key);
	void ChangeLevel(bool down);
	void SetupPlayer();
	std::string AskString(bool isName = false) const; // should be moved to utils
	SPECIES AskSpecies(bool forPlayer = true);

	/// Menu things ///
	void DrawMenu() const;
	std::string GenerateWelcome(int friendId);

	/// Creating things ///
	//void CreateRooms(int amount);
	void CreateNPCs(int amount);
	void CreateItems(int amount);
	void Cheat(const std::string& cheat) const;

	/// Clearing things ///
	void ClearNPCs();
	void ClearNPCsExit();
	void ClearItems();

	/// Variables ///
	static unsigned int m_RandomMapCount;
	const char* m_NoUpMsg;
	const char* m_NoDownMsg;
	bool m_NextTurn;
	Player m_Player;
	std::vector<Entity*> m_NPCs;
	std::vector<ItemTile> m_Items;
	//std::string m_Cheat;

	//std::vector<Map*> m_Maps;
	//Map* m_CurrentMap;
	unsigned int m_CurrentLevel;
	unsigned int m_MapNumber;

	/// Hidden stuff, go away ///
	bool dev{ false };
};

#endif // !GAME_H
