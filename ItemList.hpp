#pragma once
#ifndef _ITEMLIST_H_
#define _ITEMLIST_H_

#include <vector>
#include "Item.hpp"

class ItemList
{
public:
	explicit ItemList();
	ItemList(ItemList&& other) noexcept = delete;
	ItemList(const ItemList& other) = delete;
	ItemList& operator=(const ItemList& other) = delete;
	ItemList& operator=(ItemList&& other) noexcept = delete;
	~ItemList();


	/// FUNCTIONS ///
	Item* GetRandomItem();

	size_t GetAmountOfItems();
	size_t GetAmountOfConsumables();
	size_t GetAmountOfWeapons();
	size_t GetAmountOfArmors();
	/*
	/// ITEM		i_ ///
	static Item i_Test;
	static Item i_MacGuffin;
	static Item i_Ring;

	/// CONSUMABLE	c_ ///
	static Consumable c_Pizza;
	static Consumable c_Pasta;
	static Consumable c_HealthPotion;
	static Consumable c_Bad;

	/// WEAPON		w_ ///
	static Weapon w_Goedendag;
	static Weapon w_Sword;
	static Weapon w_Mace;
	static Weapon w_Spear;

	/// ARMOR		a_ ///
	static Armor a_WinterCoat; // tundras are cold
	static Armor a_CowboyHat;
	static Armor a_BucketHat;
	*/

	/// ARRAYS ///
	// will have to change this when loading items from files is implemented
private:
	std::vector<Item*> m_Items;
	std::vector<Consumable*> m_Consumables;
	std::vector<Weapon*> m_Weapons;
	std::vector<Armor*> m_Armors;
};

#endif // !_ITEMLIST_H_
