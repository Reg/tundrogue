#pragma once
#ifndef _ITEM_H_
#define _ITEM_H_

#include <string>
#include "structs.hpp"

enum class ITEM_TYPE : unsigned char
{
	DEFAULT,
	CONSUMABLE,
	WEAPON,
	ARMOR
};

/*
 *	Item struct for in inventories
 */
struct Item
{
	Item();
	Item(const std::string& name, unsigned int value, unsigned int mass);

	std::string itemName;
	char itemSymbol;
	unsigned int value;		// something would have to be very worthless to be < 0
	unsigned int mass;		// in kg, the only right unit for mass
	ITEM_TYPE itemType;
};

struct Consumable : public Item
{
	Consumable();
	Consumable(const std::string& name, unsigned int value, unsigned int mass, int heal);
	//Consumable(const Item& item, int heal); // eat anything?

	int consumableHeal; // can damage
};

struct Weapon : public Item
{
	Weapon();
	Weapon(const std::string& name, unsigned int value, unsigned int mass, int minDamage, int maxDamage);
	Weapon(const Item& item, int minDamage, int maxDamage); // use anything as a weapon?

	int weaponMinDamage;
	int weaponMaxDamage;
};

struct Armor : public Item
{
	Armor();
	Armor(const std::string& name, unsigned int value, unsigned int mass, int rating);

	int armorRating;
};

/*
 *	Item struct for in the world
 */
struct ItemTile
{
	ItemTile(Item* item, Position position);

	Item* item;
	Position position;
};

#endif
