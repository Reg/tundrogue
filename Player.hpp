#pragma once
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Entity.hpp"

enum class INVENTORY_CHOICE : short
{
	USE_ITEM,
	DROP_ITEM,
	GIVE_ITEM
};

enum class INTERACT_CHOICE : short
{
	FIGHT,
	HUG,
	GIVE
};

class Player : public Entity
{
public:
	explicit Player(const std::string& name);
	explicit Player(const std::string& name, SPECIES type);

	bool ChooseAction(const std::vector<Entity*>& npcs, INTERACT_CHOICE choice);
	bool OpenInventory(std::vector<ItemTile>& items, INVENTORY_CHOICE choice);
	int GetItemId(const std::string& title);

	std::vector<Entity*>& GetFriends();

	void SetPosition(int x, int y) override;
	void SetPosition(Position position) override;

	bool Move(int x, int y) override;
	bool Move(DIRECTION direction) override;
	void Draw() const override;
	void ResetPosition();
	void ResetDrawingOffset();

private:
	void Die() override;

	//const static wchar_t m_CharPlayer;
	std::vector<Entity*> m_Friends;
};

#endif // !PLAYER_H
