#include "ItemList.hpp"
#include "LuaWrapper.hpp"
#include "utils.hpp"

#ifdef _DEBUG
#include <iostream>
#endif

bool CheckLua(lua_State* L, int r)
{
    if(r != LUA_OK)
    {
    #ifdef _DEBUG
        const char* error{ lua_tostring(L, -1) };
        std::cerr << error << '\n';
    #endif
        return false;
    }
    return true;
}


ItemList::ItemList()
    : m_Items{}
    , m_Consumables{}
    , m_Weapons{}
    , m_Armors{}
{
    lua_State* L{ luaL_newstate() };

    if(CheckLua(L, luaL_dofile(L, "data/lua/items.lua")))
    {
        // get items
        lua_getglobal(L, "items");

        lua_len(L, -1);
        lua_pop(L, 1); // pop len
        m_Items.reserve(int(lua_tointeger(L, -1)));

        int tableIdx{ lua_gettop(L) };
        lua_pushnil(L);
        while(lua_next(L, tableIdx) != 0)
        {
            if(!lua_istable(L, -1))
            {
            #ifdef _DEBUG
                std::cerr << "Position " << std::to_string(m_Items.size()) << ": Couldn't find an Item table\n";
            #endif
                continue;
            }

            const char* name;
            unsigned int value;
            unsigned int mass;

            lua_pushstring(L, "name");
            lua_gettable(L, -2);
            name = lua_tostring(L, -1);
            lua_pop(L, 1);

            lua_pushstring(L, "value");
            lua_gettable(L, -2);
            value = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "mass");
            lua_gettable(L, -2);
            mass = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            // pop the current item table
            lua_pop(L, 1);

            m_Items.push_back(new Item{ name, value, mass });
        }

        // get consumables
        lua_getglobal(L, "consumables");

        lua_len(L, -1);
        lua_pop(L, 1); // pop len
        m_Consumables.reserve(int(lua_tointeger(L, -1)));

        tableIdx = lua_gettop(L);
        lua_pushnil(L);
        while(lua_next(L, tableIdx) != 0)
        {
            if(!lua_istable(L, -1))
            {
            #ifdef _DEBUG
                std::cerr << "Position " << std::to_string(m_Consumables.size()) << ": Couldn't find an Item table\n";
            #endif
                continue;
            }

            const char* name;
            unsigned int value;
            unsigned int mass;
            int heal;

            lua_pushstring(L, "name");
            lua_gettable(L, -2);
            name = lua_tostring(L, -1);
            lua_pop(L, 1);

            lua_pushstring(L, "value");
            lua_gettable(L, -2);
            value = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "mass");
            lua_gettable(L, -2);
            mass = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "heal");
            lua_gettable(L, -2);
            heal = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            // pop the current item table
            lua_pop(L, 1);

            m_Consumables.push_back(new Consumable{ name, value, mass, heal });
        }

        // get weapons
        lua_getglobal(L, "weapons");

        lua_len(L, -1);
        lua_pop(L, 1); // pop len
        m_Weapons.reserve(int(lua_tointeger(L, -1)));

        tableIdx = lua_gettop(L);
        lua_pushnil(L);
        while(lua_next(L, tableIdx) != 0)
        {
            if(!lua_istable(L, -1))
            {
            #ifdef _DEBUG
                std::cerr << "Position " << std::to_string(m_Weapons.size()) << ": Couldn't find an Item table\n";
            #endif
                continue;
            }

            const char* name;
            unsigned int value;
            unsigned int mass;
            int minDamage;
            int maxDamage;

            lua_pushstring(L, "name");
            lua_gettable(L, -2);
            name = lua_tostring(L, -1);
            lua_pop(L, 1);

            lua_pushstring(L, "value");
            lua_gettable(L, -2);
            value = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "mass");
            lua_gettable(L, -2);
            mass = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "minDamage");
            lua_gettable(L, -2);
            minDamage = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "maxDamage");
            lua_gettable(L, -2);
            maxDamage = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            // pop the current item table
            lua_pop(L, 1);

            m_Weapons.push_back(new Weapon{ name, value, mass, minDamage, maxDamage });
        }

        // get armors
        lua_getglobal(L, "armors");

        lua_len(L, -1);
        lua_pop(L, 1); // pop len
        m_Armors.reserve(int(lua_tointeger(L, -1)));

        tableIdx = lua_gettop(L);
        lua_pushnil(L);
        while(lua_next(L, tableIdx) != 0)
        {
            if(!lua_istable(L, -1))
            {
            #ifdef _DEBUG
                std::cerr << "Position " << std::to_string(m_Armors.size()) << ": Couldn't find an Item table\n";
            #endif
                continue;
            }

            const char* name;
            unsigned int value;
            unsigned int mass;
            int rating;

            lua_pushstring(L, "name");
            lua_gettable(L, -2);
            name = lua_tostring(L, -1);
            lua_pop(L, 1);

            lua_pushstring(L, "value");
            lua_gettable(L, -2);
            value = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "mass");
            lua_gettable(L, -2);
            mass = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            lua_pushstring(L, "rating");
            lua_gettable(L, -2);
            rating = unsigned(lua_tointeger(L, -1));
            lua_pop(L, 1);

            // pop the current item table
            lua_pop(L, 1);

            m_Armors.push_back(new Armor{ name, value, mass, rating });
        }
    }
}

ItemList::~ItemList()
{
    for(auto& item : m_Items)
    {
        delete item;
    }

    for(auto& consumable : m_Consumables)
    {
        delete consumable;
    }

    for(auto& weapon : m_Weapons)
    {
        delete weapon;
    }

    for(auto& armor : m_Armors)
    {
        delete armor;
    }
}

/*ItemList::ItemList(ItemList&& other) noexcept
    : m_Items{ std::move(other.m_Items) }
    , m_Consumables{ std::move(other.m_Consumables) }
    , m_Weapons{ std::move(other.m_Weapons) }
    , m_Armors{ std::move(other.m_Armors) }
{
}*/

/*
/// ITEM ///
Item ItemList::i_Test{ "Test", 250, 5 };
Item ItemList::i_MacGuffin{ "MacGuffin", 2500, 5 };
Item ItemList::i_Ring{ "Ring", 25000, 5 };

/// CONSUMABLE ///
Consumable ItemList::c_Pizza{ "Pizza", 12, 1, 15 };
Consumable ItemList::c_Pasta{ "Pasta", 12, 1, 15 };
Consumable ItemList::c_HealthPotion{ "Health Potion", 10, 1, 15 };
Consumable ItemList::c_Bad{ "Something bad", 10, 1, -15 };

/// WEAPON ///
Weapon ItemList::w_Goedendag{ "Goedendag", 250, 5, 6, 30 };
Weapon ItemList::w_Sword{ "Sword", 250, 5, 5, 20 };
Weapon ItemList::w_Mace{ "Mace", 250, 5, 5, 25 };
Weapon ItemList::w_Spear{ "Spear", 250, 5, 5, 21 };

/// ARMOR ///
Armor ItemList::a_WinterCoat{ "Winter Coat", 500, 0, 6 };
Armor ItemList::a_CowboyHat{ "Cowboy Hat", 500, 0, 5 };
Armor ItemList::a_BucketHat{ "Bucket Hat", 500, 0, 5 };
*/

/// ARRAYS ///
/*
Item* ItemList::items[ItemList::itemAmount] = { &i_MacGuffin, &i_Ring };
Consumable* ItemList::consumables[ItemList::consumableAmount] = { &c_Pizza, &c_Pasta, &c_HealthPotion, &c_Bad };
Weapon* ItemList::weapons[ItemList::weaponAmount] = { &w_Goedendag, &w_Sword, &w_Mace, &w_Spear };
Armor* ItemList::armors[ItemList::armorAmount] = { &a_WinterCoat, &a_CowboyHat, &a_BucketHat };*/

Item* ItemList::GetRandomItem()
{
    //return items[utils::RandomBetween(0, items.size() - 1)];

    int type{ utils::RandomBetween(0, 3) };
    switch(type)
    {
        case 0:
        {
            int id{ utils::RandomBetween(0, m_Items.size() - 1) };
            return m_Items[id];
            break;
        }
        case 1:
        {
            int id{ utils::RandomBetween(0, m_Consumables.size() - 1) };
            return m_Consumables[id];
            break;
        }
        case 2:
        {
            int id{ utils::RandomBetween(0, m_Weapons.size() - 1) };
            return m_Weapons[id];
            break;
        }
        case 3:
        {
            int id{ utils::RandomBetween(0, m_Armors.size() - 1) };
            return m_Armors[id];
            break;
        }
    }
}

size_t ItemList::GetAmountOfItems()
{
    return m_Items.size();
}

size_t ItemList::GetAmountOfConsumables()
{
    return m_Consumables.size();
}

size_t ItemList::GetAmountOfWeapons()
{
    return m_Weapons.size();
}

size_t ItemList::GetAmountOfArmors()
{
    return m_Armors.size();
}
