#include "Item.hpp"
//#include "RLGame.hpp"

/// ITEM ///
Item::Item()
	: itemName{ "Unnamed Item" }
	, itemSymbol{}
	, value{}
	, mass{}
	, itemType{ ITEM_TYPE::DEFAULT }
{
}

Item::Item(const std::string& name, unsigned int value, unsigned int mass)
	: itemName{ name }
	, itemSymbol{ '%' }
	, value{ value }
	, mass{ mass }
	, itemType{ ITEM_TYPE::DEFAULT }
{
}

/// CONSUMABLE ///
Consumable::Consumable()
	: Consumable("Unnamed Consumable", 0, 0, 0)
{
}

Consumable::Consumable(const std::string& name, unsigned int value, unsigned int mass, int heal)
	: Item(name, value, mass)
	, consumableHeal{ heal }
{
	itemSymbol = '*';
	itemType = ITEM_TYPE::CONSUMABLE;
}

/// WEAPON ///
Weapon::Weapon()
	: Weapon("Unnamed Weapon", 0, 0, 0, 0)
{
}

Weapon::Weapon(const std::string& name, unsigned int value, unsigned int mass, int minDamage, int maxDamage)
	: Item(name, value, mass)
	, weaponMinDamage{ minDamage }
	, weaponMaxDamage{ maxDamage }
{
	itemSymbol = '/';
	itemType = ITEM_TYPE::WEAPON;
}

Weapon::Weapon(const Item& item, int minDamage, int maxDamage)
	: Weapon(item.itemName, item.value, item.mass, minDamage, maxDamage)
{
}

/// ARMOR ///
Armor::Armor()
	: Armor("Unnamed Armor", 0, 0, 0)
{
}

Armor::Armor(const std::string& name, unsigned int value, unsigned int mass, int rating)
	: Item(name, value, mass)
	, armorRating{ rating }
{
	itemSymbol = '^';
	itemType = ITEM_TYPE::ARMOR;
}

/// ITEMTILE ///
ItemTile::ItemTile(Item* item, Position position)
	: item{ item }
	, position{ position }
{
}
